using NUnit.Framework;
using Playground.Lib;
using Xunit;
using Assert = NUnit.Framework.Assert;

namespace Playground.Tests
{
    [TestFixture]
    public class FactorialCalculatorTests
    {
        private FactorialCalculator _target;

        public FactorialCalculatorTests()
        {
            Setup();
        }
       
        [SetUp]
        public void Setup()
        {
            _target = new FactorialCalculator();
        }

        [Fact]
        [Test]
        public void CalculateFactorial_TestCase1()
        {
            ulong input = 5;
            ulong expected = 120;

            var result = _target.CalculateFactorial(input);

            Assert.AreEqual(expected, result);
        }
    }
}