﻿namespace Playground.Lib
{
    public class FactorialCalculator
    {
        public ulong CalculateFactorial(ulong n)
        {
            if (n < 2)
            {
                return 1;
            }
            return n * CalculateFactorial(n - 1);
        }
    }
}