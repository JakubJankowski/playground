﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Playground.Web.Startup))]
namespace Playground.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
